(ns slack-to-html-export.core
  (:require [cheshire.core :as cheshire]
            [hiccup.core :as h]
            [hiccup.util :as hu]
            [clj-time.coerce :as c]
            [clj-time.format :as f]
            [clojure.string :as str]))

(def slack-export-folder "the_enriched_slack_export")

(def users (group-by :id (cheshire/parse-string (slurp (str slack-export-folder "/users.json")) true)))
(def channels (cheshire/parse-string (slurp (str slack-export-folder "/channels.json")) true))

(defn- write-file [file-name html-tree]
  (spit (str slack-export-folder "/" file-name)
    (h/html html-tree)))

(defn- channel-files [channel-name]
  "Return a sorted list of JSON files for a given channel."
  (let [directory (clojure.java.io/file (str slack-export-folder "/" channel-name))
        files (file-seq directory)]
    (->> files
      (filter #(.isFile %))
      (map #(.getName %))
      #_(filter #(= (:type %) "message"))
      (sort))))

(defn- render-date-time [timestamp]
  (->> (read-string (str (first (str/split timestamp #"\.")) "000"))
    c/from-long
    (f/unparse (f/formatters :date-hour-minute-second))))

(defn fix-links
  [text]
  "Parses the <...> parts of each message and then tries to match them against the users, etc.."
  (str/replace
    text
    #"\<([\@\!\:\/\-\.\#\_\?\%\=\&\"\|a-zA-Z0-9]+)\>"
    (fn [match]
      (let [prefix (first (nth match 1))]
        (cond
          (= \@ prefix) (let [user (first (get users (apply str (rest (nth match 1)))))]
                          (str "@" (:real_name (:profile user))))
          (= \! prefix) (str "@" (apply str (rest (nth match 1))))

          (str/starts-with? (nth match 1) "http") (str "<a href=\"" (nth match 1) "\">" (nth match 1) "</a>")

          :else
          (hu/escape-html (str "<" (nth match 1) ">")))))))

(defn fix-code-blocks
  [text]
  "Parses the ```...``` and turns it into a <pre>...</pre> block"
  (str/replace
    text
    #"\`\`\`([^\`]*)\`\`\`"
    (fn [match]
      (str "<pre>" (nth match 1) "</pre>"))))

(defn- create-file-links [files]
  (when files
    (str/join
      ","
      (for [file files]
        (str " <a href=\"__uploads/" (:id file) "/" (:name file) "\">" (:name file) "</a>")))))

(defn- process-channel [channel-name]
  (let [cfs (channel-files channel-name)]
    (for [message (->> cfs
                    (map (fn [cf]
                           (cheshire/parse-string (slurp (str slack-export-folder "/" channel-name "/" cf)) true)))
                    (flatten)
                    (sort-by :ts))]
      (let [user (first (get users (:user message)))
            files (:files message)]
        [:div.row.message {:id (str channel-name "_" (:ts message))}
         ;; avatar image
         [:div.col-sm-1.avatar
          [:img {:src (:image_48 (:profile user))}]]

         ;; the message itself
         [:div.col-sm-11
          [:div.row
           [:div.col-sm-12
            [:span.username (:real_name (:profile user))]
            [:span.datetime (render-date-time (:ts message))]]]
          [:div.row
           [:div.col-sm-12
            (str
              (fix-code-blocks (fix-links (:text message)))
              (create-file-links files))]]]]))))

(time
  (dorun
    (for [channel channels]
      (let [title (str "Channel #" (:name channel))]
        (write-file (str (:name channel) ".html")
          (list
            [:head
             [:title title]
             [:link {:rel "stylesheet"
                     :href "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
                     :integrity "sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
                     :crossorigin "anonymous"}]
             [:link {:rel "stylesheet"
                     :href "app.css"}]
             ]
            [:body
             [:div.container
              [:h1 title]
              (process-channel (:name channel))]]))))))



